SHELL = /bin/bash

.PHONY: default
default:

.PHONY: install-composer
install-composer:
	scripts/install-composer.sh
	mv composer.phar bin/composer
