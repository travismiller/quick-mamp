# quick-mamp

A quick boilerplate for PHP on a Mac with MAMP integration.

## Resources

- [composer](https://getcomposer.org/)
- [direnv](https://direnv.net/)
- [EditorConfig](https://editorconfig.org/)
- [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
- [MAMP](https://www.mamp.info/en/)
- [.editorconfig](https://gitlab.com/snippets/1837991) configuration snippet
- [.mamp](https://gitlab.com/snippets/1880297) environment detection snippet

## License

This software is licensed under the permissive [MIT license](LICENSE).
