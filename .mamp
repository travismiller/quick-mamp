#!/bin/bash -e

MAMP_BASE="${MAMP_BASE:-"/Applications/MAMP"}"
MAMP_PHP_MAJOR="${MAMP_PHP_MAJOR:-7}"
MAMP_PHP_MINOR="${MAMP_PHP_MINOR:-3}"
MAMP_BIN_ENABLED="${MAMP_BIN_ENABLED:-true}"
MAMP_PHP_ENABLED="${MAMP_PHP_ENABLED:-true}"

function mamp_php {
  local MAMP_PHP_BASE="$MAMP_BASE/bin/php"
  local PHP_ALL_VERSIONS=$(ls "$MAMP_PHP_BASE" | grep "php")
  local PHP_MINOR_VERSIONS=$(ls "$MAMP_PHP_BASE" | grep "php${MAMP_PHP_MAJOR}\.${MAMP_PHP_MINOR}\.")
  local PHP_VERSION=$(echo "$PHP_MINOR_VERSIONS" | sort -t. -k3 -g | tail -1)

  if [ -z "$PHP_VERSION" ]; then
    local PHP_VERSIONS_AVAILABLE=$(echo "$PHP_ALL_VERSIONS" | sed 's/^php/  /g')
    (>&2 echo "Invalid PHP version:")
    (>&2 echo "  $PHP_MAJOR.$PHP_MINOR")
    (>&2 echo "Available PHP versions in MAMP:")
    (>&2 echo "  $PHP_VERSIONS_AVAILABLE")
  else
    echo "$MAMP_PHP_BASE/$PHP_VERSION/bin"
  fi
}

function mamp_bin {
  echo "$MAMP_BASE/Library/bin"
}

if [ -d "$MAMP_BASE" ]; then
  if [ "$MAMP_BIN_ENABLED" = "true" ]; then
    PATH="$(mamp_bin):$PATH"
  fi

  if [ "$MAMP_PHP_ENABLED" = "true" ]; then
    PATH="$(mamp_php):$PATH"
  fi

  export MAMP_BASE
  export MAMP_PHP_MAJOR
  export MAMP_PHP_MINOR
  export MAMP_BIN_ENABLED
  export MAMP_PHP_ENABLED
  export PATH
fi
